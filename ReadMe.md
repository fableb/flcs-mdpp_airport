# MDPP (POP) Gregorio Luperón International Airport Scenery

This is Puerto Plata MDPP airport scenery for FlightGear Flight Simulator with

- Interior design
- Day/night lightmaps
- Ground net
- Animated jetways
- ...

## Installation

- Download the repository .zip file with the link '...' -> 'Download repository'
- Extract the downloaded file to a directory of your choice
- Add this directory to 'Add-ons' -> 'Additional scenery folders' in FlightGear GUI launcher.

## Screenshots

![...](./media/fgfs-20231115010946.png)

![...](./media/Screenshot_20210516_073928.png)

![...](./media/fgfs-20211120161323.png)

![...](./media/fgfs-20211120161538.png)

![...](./media/fgfs-20211120161938.png)

![...](./media/Screenshot_20210512_080609.png)

![...](./media/Screenshot_20210512_080650.png)

![...](./media/Screenshot_20210512_080721.png)

![...](./media/Screenshot_20210516_073710.png)


![...](./media/.png)

## List of Airports I am Working On:

- MDPP - Gregorio Luperón International Airport, Puerto Plata Airport: https://bitbucket.org/fableb/flcs-mdpp_airport/src/master/
- MDSD - Las Américas International Airport: https://bitbucket.org/fableb/flcs-mdsd_airport/src/master/
- KMIA - Miami International Airport: https://bitbucket.org/fableb/flcs-kmia_airport/src/master/
- KWER - Newark Liberty International Airport: https://bitbucket.org/fableb/flcs-kewr_airport/src/master/
- TFFR - Pointe-à-Pitre International Airport: https://bitbucket.org/fableb/flcs-tffr_airport/src/master/

